const Joi = require('joi'); 
 
const validation = (schema) =>{
    return ((req, res, next) => {
        Joi.validate(req.body, schema, function (error, value) {
            if(error) {
                res.locals.errors = {
                    "message": error.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage)
            } 
            if(!error) next()
        })  
    })
}
 
const schema = {
    login : Joi.object().keys({
        email: Joi.string().min(3).max(15).required(),
        password:Joi.string().min(2).max(15).required()
    })
}
 
module.exports = { validation, schema }