const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');


        router.route('/create')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'สั่งซื้อสินค้า'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }
            req.renderPage = "pages/purchases/addForm"       
            next()
        })
        .get((req, res, next) => { 
            connection.query('SELECT * FROM warehouses',function(err,rows) {
                if(err){
                 req.flash('error', err); 
                }else{
                 //res.render({page_title:"แบรนด์สินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});
                 res.render('pages/purchases/addForm',{page_title:"สั่งซื้อสินค้า",data:rows});
                }                      
            });
  
        })

        router.route('/detail/:order_id')
        .get((req, res, next) => { 
        
            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }
            //console.log(req.params.order_id)    
            var order_id = req.params.order_id;
            connection.query('SELECT * FROM purchases INNER JOIN order_status ON purchases.order_status = order_status.status_id INNER JOIN courier ON purchases.shipping_code = courier.courier_code WHERE purchases.id = $1 AND purchases.warehouse_id = $2',[order_id, req.session.wh_id],function(err,result) {
                connection.query('SELECT purchases_items.*,products.product_name FROM purchases_items LEFT JOIN products ON purchases_items.product_sku = products.product_sku WHERE purchases_items.purchase_id = $1',[order_id],function(err,orderItem) {
                    connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,whid) {
                        connection.query('SELECT * FROM config',function(err,awb) {
                            if(err){
                                console.log('error page',err)
                                return res.render('pages/purchases/detail',{page_title:"Order Detail",data:''});   
                            }else{
                                return res.render('pages/purchases/detail',{page_title:"Order Detail",data:result,itemorder:orderItem,warehouse:whid,company:awb})    
                            }                      
                        });
                    });
                });
            });
        })
        router.route('/invoice/:order_id')
        .get((req, res, next) => { 
        
            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }
            //console.log(req.params.order_id)    
            var order_id = req.params.order_id;
        
            connection.query('SELECT * FROM purchases INNER JOIN order_status ON purchases.order_status = order_status.status_id INNER JOIN courier ON purchases.shipping_code = courier.courier_code WHERE purchases.id = $1 AND purchases.warehouse_id = $2',[order_id, req.session.wh_id],function(err,result) {
                connection.query('SELECT purchases_items.*,products.product_name FROM purchases_items LEFT JOIN products ON purchases_items.product_sku = products.product_sku WHERE purchases_items.purchase_id = $1',[order_id],function(err,orderItem) {
                    connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,whid) {
                        if(err){
                            console.log('error page',err)
                            return res.render('pages/purchases/invoice',{page_title:"Order Detail",data:''});   
                        }else{
                            return res.render('pages/purchases/invoice',{page_title:"Order Detail",data:result,itemorder:orderItem,warehouse:whid})    
                        }                      
                    });
                });
            });    
        })
        
    router.route('/lists')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'รายการสั่งซื้อสินค้า'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }

        

        connection.query('SELECT * FROM purchases INNER JOIN purchases_items ON purchases.id = purchases_items.purchase_id INNER JOIN products ON purchases_items.product_sku = products.product_sku ORDER BY purchase_id;',function(err,rows) {
        //connection.query('SELECT O.invoice, CONVERT(date,O.OrderDate) AS Date, P.product_name, I.quantity FROM [purchases] O JOIN purchases_items I ON O.id = I.purchase_id JOIN products P ON P.product_sku = I.product_sku ORDER BY O.invoice',function(err,rows) {
        //connection.query('SELECT * FROM purchases ORDER BY id ASC',function(err,rows) {
            if(err){
            console.log(err)
             res.render('pages/purchases/ListsofPurchases',{data:''});    
            }else{
             res.render('pages/purchases/ListsofPurchases',{page_title:"รายการสั่งซื้อสินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});

            }                      
        });

    })

    
    router.route('/add')
    .post(type,(req, res, next) => { 

        /* console.log('order insert',req) */

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;
        Object.create(null)
        const schema = Joi.object().keys({

            //warehouse_id: Joi.string().required(),
            //invoice: Joi.string().required(),
            //order_status: Joi.string().required(),
            //totalPrice: Joi.string().required(),
            shipping_firstname: Joi.string().required(),
            shipping_lastname: Joi.string().required(),
            shipping_address1: Joi.string().required(),
            shipping_address2: Joi.string().required(),
            shipping_city: Joi.string().required(),
            shipping_state: Joi.string().required(),
            shipping_country: Joi.string().required(),
            shipping_postcode: Joi.string().required(),
            shipping_telephone: Joi.string().required(),
            sku_product:Joi.array().required(),
            qty_product:Joi.array().required(),
            price_product:Joi.array().required(),
            //sku_product: Joi.string().required(),
            //qty_product: Joi.string().required(),
            //price_product: Joi.string().required(),

        });

        Joi.validate(data, schema, (err, value) => {

            let sku_product_array = req.body.sku_product;
            let qty_product_array = req.body.qty_product;
            let price_product_array = req.body.price_product;

            /* let name_product_join = name_product_array.join();
            let qty_product_join = qty_product_array.join();
            let price_product_one_join = price_product_one_array.join();
            let price_product_join = price_product_array.join(); */
            

            //console.log('order insert',value,' ',name_product_join)

            if (err) {
                //console.log(err)
                res.locals.errors = {
                   "message": err.details[0].message
               }
               res.locals.user = req.body
               return res.render(req.renderPage);
                

            } else {
                
                //console.log('insert orders')
               /*  const orders_save = {
                    order_status:req.body.order_status,
                    order_date:new Date(),
                    shipped_date:new Date(),
                    created_at:new Date(),
                  
                } */
               
                let inv = 'INV0001';
                let order_status = 1;
                let total_price = 33333
                console.log(req.body)
                connection.query('INSERT INTO purchases (warehouse_id, invoice, order_status, total_price, shipping_firstname, shipping_lastname, shipping_address, shipping_address2, shipping_city, shipping_state, shipping_country, shipping_postcode, shipping_telephone, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14) RETURNING *', [req.session.wh_id,inv,order_status,total_price, req.body.shipping_firstname, req.body.shipping_lastname, req.body.shipping_address1, req.body.shipping_address2, req.body.shipping_city, req.body.shipping_state, req.body.shipping_country, req.body.shipping_postcode, req.body.shipping_telephone, new Date()] , (error, results) => {
                    
                    if (error) {
                      throw error
                    }else{
                        
                         /* console.log('insert order_items',results) */

                         for(var i = 0; i < sku_product_array.length;i++){
                            //console.log(sku_product_array[i])
                            connection.query('INSERT INTO purchases_items (purchase_id, product_sku, quantity, totalprice ,created_at) VALUES ($1, $2, $3, $4, $5)', [results.rows[0].id, sku_product_array[i], qty_product_array[i], price_product_array[i], new Date()], (error, results) => {

                                //console.log('insert order_items',error)
                               /*  if(error){

                                    throw error

                                }else{
                                   
                                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                    res.redirect('/admin/me');
                                    res.end(results);
                                } */
                                 try {
                                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                    res.redirect('/admin/me');
                                
                                   
                                  } catch (error) {
                                    //console.error(error);
                                  }

                             });
                             
                           
                         }

                    }
                    
                });
      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            brand_name: Joi.string().min(3).max(30).required(),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM brands WHERE brand_id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/purchases',{page_title:"รายการสั่งซื้อสินค้า",data:rows});
                            
                });

            } else {

                var sql = "UPDATE brands set brand_name=?  WHERE brand_id = ?";
 
                var query = connection.query(sql, [req.body.brand_name, req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'แก้ไขข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router