const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');


        router.route('/form_users').all((req, res, next) => { 

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            return res.render('pages/users/form_users',{name:req.session.nameuser});      
            
        })
        
        

    router.route('/list_users').get((req, res, next) => { 

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);

        res.locals.pageData = {
            title:'users'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }

        var sql = 'SELECT * FROM users WHERE warehouse_id = $1 ORDER BY users_id ASC  ';
       
    
        connection.query(sql,[req.session.wh_id],function(err,rows) {
            //console.log('what error',err)
            if(err){
            // req.flash('error', err); 
             res.render('pages/users/list_users',{page_title:"Users List",data:''});   
            }else{
             res.render('pages/users/list_users',{page_title:"Users List",data:rows, admin:req.session.adminid,name:req.session.nameuser});



            }                      
        });

    })

    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');
        const data = req.body;
        const schema = Joi.object().keys({

            user_name: Joi.required(),
            password: Joi.required(),
            roles: Joi.required(),
        });



        Joi.validate(data, schema, (err, value) => {

        
            if (err) {

                res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);

            } else {
                
                connection.query('INSERT INTO users (users_name,password,warehouse_id,roles) VALUES ($1, $2, $3, $4)', [req.body.user_name,req.body.password,req.session.wh_id,req.body.roles],(error, results) => {

                    console.log('dddd',error)
                    if (error) {
                        throw error
                      }
                    res.redirect('/users/list_users');
                });
      
            }

        });
    })

    
    router.route('/update/')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            first_name: Joi.required(),
            last_name: Joi.required(),
            status: Joi.required(),
            users_id:Joi.required(),
            phone:Joi.required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                //console.log('err users',err)
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {

               // console.log('users===',req.body.status,req.body.level,req.body.users_id)
             
               
                connection.query('UPDATE distributor_login SET status = $1 , firstname = $2 , lastname = $3 WHERE id = $4 ', [req.body.status,req.body.first_name,req.body.last_name,req.body.users_id], (error, results) => {
                       
                    if(req.body.phone){

                        connection.query('UPDATE distributor_telephone SET telephone = $1 WHERE id = $2 ', [req.body.phone,req.body.users_id], (error, results) => {

                              if (error) {
                                throw error
                              }else{
          
                                  res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                  res.redirect('/users/list_users');
                              }

                        });

                    }else{

                          if (error) {
                            throw error
                          }else{
      
                              res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                              res.redirect('/users/list_users');
                          }

                    }
                   
                    
                });
      
            }
        });
    })

    /* router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(30).required(),
            email: Joi.string().allow('', null),
            password: Joi.string().min(6).max(30).required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers WHERE id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/customers',{page_title:"User Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE customers set name=?, password =?, telephone =?, line_id =?, updated_at=?  WHERE id = ?";
 
                var query = connection.query(sql, [req.body.name, req.body.password, req.body.telephone, req.body.line_id, new Date(), req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    }) */

module.exports = router