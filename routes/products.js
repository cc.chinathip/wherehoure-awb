const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');
const helpers = require('../helpers/newlib');

        router.route('/add_products')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            req.renderPage = "pages/products/form_products"       
            next()
        })
        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            connection.query('SELECT * FROM products ',function(err,results)  {
           
               // res.render('pages/products/form_products',{name:req.session.nameuser,data:results.rows});   

              // console.log('results',results)

                return res.render('pages/products/form_products',{name:req.session.nameuser,data:results.rows});

            });
        })


    router.route('/list_products')
    .get((req, res, next) => { 

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);

        res.locals.pageData = {
            title:'Products'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
       /*  console.log('now month',helpers.GetCurrentMonth()) */
       async function main () {

            try {
        
                
                let order_current_month = await connection.query('SELECT SUM(purchases_items.quantity) as total_order FROM purchases LEFT JOIN purchases_items ON purchases_items.purchase_id = purchases.id WHERE purchases.warehouse_id = $1 AND EXTRACT(month FROM purchases_items.created_at) = $2',[req.session.wh_id,helpers.GetCurrentMonth()])
                let order_today = await connection.query('SELECT SUM(purchases_items.quantity) as total_order FROM purchases LEFT JOIN purchases_items ON purchases_items.purchase_id = purchases.id WHERE purchases.warehouse_id = $1 AND purchases_items.created_at = now()  ',[req.session.wh_id])
                let stock_now = await connection.query('SELECT SUM(purchases_items.quantity) as total_order FROM purchases LEFT JOIN purchases_items ON purchases_items.purchase_id = purchases.id WHERE purchases.warehouse_id = $1',[req.session.wh_id])
                let total_product = await connection.query('SELECT SUM(stocks.quantity) as total_product FROM products LEFT JOIN stocks ON products.product_id=stocks.product_id WHERE stocks.warehouse_id = $1',[req.session.wh_id])
                let product_by_warehourse = await connection.query('SELECT products.*,stocks.quantity FROM products LEFT JOIN stocks ON products.product_id=stocks.product_id WHERE stocks.warehouse_id = $1',[req.session.wh_id])
                
                let datas = {
        
                    page_title:"Products List",
                    admin:req.session.adminid,
                    name:req.session.nameuser,
                    data:product_by_warehourse.rows,
                    order_last_month: order_current_month.rows[0].total_order ? order_current_month.rows[0].total_order : 0,
                    order_today:order_today.rows[0].total_order ? order_today.rows[0].total_order : 0,
                    total_order:stock_now.rows[0].total_order ? helpers.numberWithCommas(stock_now.rows[0].total_order) : 0,
                    total_product:total_product.rows[0].total_product?total_product.rows[0].total_product:0,
                    sum_total_order:total_product.rows[0].total_product ? helpers.numberWithCommas(total_product.rows[0].total_product - stock_now.rows[0].total_order) : 0,
                    
                }
        
                //console.log('datas---',datas)
                res.render('pages/products/stocks',datas);
        
                // do somethings
            
            } catch (error) {
                // handling user error
                
                console.log('error', error)
                res.render('pages/products/stocks',{page_title:"Products List",data:''}); 
            }
        
        }

        main()

    })

    router.route('/list_products_api')
    .get((req, res, next) => { 


        connection.query('SELECT products.* FROM products ',function(err,rows) {
            if(err){
            //req.flash('error', err); 
                console.log('error', err)
                /* res.render('pages/products/list_products',{page_title:"Products List",data:''}); */  
                res.render('pages/products/stocks',{page_title:"Products List",data:''}); 
            }else{
               

                res.status(200).json({jsondata:rows.rows})
                
            
            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            product_name: Joi.required(),
            product_qty: Joi.required(),
        });

        Joi.validate(data, schema, (err, value) => {

            //console.log('add product log',err)
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {

                connection.query('INSERT INTO stocks (warehouse_id, product_id, quantity , created_at) VALUES ($1, $2, $3, $4)', [1,req.body.product_name, req.body.product_qty,new Date()], (error, results) => {
                           
                    console.log('results product',results,'or =',error)
                    if (error) {
                      throw error
                    }

                  
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/products/list_products');
                });

      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(30).required(),
            email: Joi.string().allow('', null),
            password: Joi.string().min(6).max(30).required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers WHERE id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/customers',{page_title:"User Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE customers set name=?, password =?, telephone =?, line_id =?, updated_at=?  WHERE id = ?";
 
                var query = connection.query(sql, [req.body.name, req.body.password, req.body.telephone, req.body.line_id, new Date(), req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router