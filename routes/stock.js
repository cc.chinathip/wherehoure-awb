const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');

    
        router.route('/list_stock')
        .all((req, res, next) => { 

            console.log('res',res)

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            req.renderPage = "pages/stock/stocks"       
            next()
        })
        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            res.render('pages/stock/stocks',{name:req.session.nameuser});   
        })


    router.route('/list_products')
    .get((req, res, next) => { 

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3500');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);

        res.locals.pageData = {
            title:'Products'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM products',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/products/list_products',{page_title:"Products List",data:''});   
            }else{
             res.render('pages/products/list_products',{page_title:"Products List",data:rows, admin:req.session.adminid,name:req.session.nameuser});

            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            product_name: Joi.required(),
            product_sku: Joi.required(),
            product_price: Joi.required(),
            product_qty: Joi.required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {
                if (!req.file) {
                    res.locals.errors = {
                        "message": 'กรุณาอัพโหลดรูปสินค้า'
                    }
                    res.locals.user = req.body
                    return res.render(req.renderPage);
                } else {
                    var tmp_path = req.file.path;
                    var target_path = 'uploads/' + req.file.originalname;
                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);
                    src.on('end', function() { 
                        
                        connection.query('INSERT INTO products (product_name, product_sku, product_price, product_qty, product_image , created_at) VALUES ($1, $2, $3, $4, $5, $6)', [req.body.product_name, req.body.product_sku, req.body.product_price, req.body.product_qty,target_path,new Date()], (error, results) => {
                           
                            //console.log('results product',results)
                            if (error) {
                              throw error
                            }
                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                            res.redirect('/products/list_products');
                        });

                    });
                    src.on('error', function(err) { 
                        res.cookie('flash_register_message_error', 'Save Data Error !!',{maxAge:3000});
                        res.redirect('pages/stock/stocks');
                    });
                }
      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(30).required(),
            email: Joi.string().allow('', null),
            password: Joi.string().min(6).max(30).required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers WHERE id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/customers',{page_title:"User Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE customers set name=?, password =?, telephone =?, line_id =?, updated_at=?  WHERE id = ?";
 
                var query = connection.query(sql, [req.body.name, req.body.password, req.body.telephone, req.body.line_id, new Date(), req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router