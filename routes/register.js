const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');

var type = upload.single('image');
 
router.route('/')
    .all((req, res, next) => { 

        res.locals.pageData = {
            title:'Register Page'
        }
        res.locals.user = {
            name:'',
            email:'',
            telephone:'',
        }

        req.renderPage = "pages/register"       
        next()
    })
    .get((req, res, next) => { 
        if(req.cookies.flash_register_message){
            res.locals.register_success = {
                message:'Save Data Successful'
            }
        }
        if(req.cookies.flash_register_message_error){
            res.locals.errors = {
                message:'Save Not Successful'
            }
        }

        res.render('pages/register',{name:req.session.nameuser});   
    })

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            username: Joi.string().min(3).max(30).required(),
            password: Joi.string().min(6).max(30).required(),
            firstname: Joi.string().min(3).max(30).required(),
            lastname: Joi.string().min(3).max(30).required(),
            email: Joi.string().email().required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {
                if (!req.file) {
                    res.locals.errors = {
                        "message": 'กรุณาอัพโหลดเอกสารบัตรประชาชน'
                    }
                    res.locals.user = req.body
                    return res.render(req.renderPage);
                } else {
                    var tmp_path = req.file.path;
                    var target_path = 'uploads/' + req.file.originalname;
                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);
                    src.on('end', function() { 
                        
                        connection.query('SELECT username FROM distributor_login WHERE username = $1', [req.body.username], function (err, result) {
                            if (err) {
                                throw err
                              }
                              if (result.rows) { 
                                if(result.rows.length > 0) {
            
                                    res.locals.errors = {
                                        "message": 'username มีในระบบแล้ว'
                                    }
            
                                    return res.render(req.renderPage);
            
                                } else {
                                    
                                    let password_hash = md5(req.body.password);

                                    connection.query('INSERT INTO distributor_login (firstname, lastname, username, password, status, date_added) VALUES ($1, $2, $3, $4, $5, $6)', [req.body.firstname, req.body.lastname, req.body.username, password_hash, 0, new Date()], (error, results) => {
                                        if (error) {
                                          throw error
                                        }

                                        connection.query('INSERT INTO distributor_file (username, file, date_added) VALUES ($1, $2, $3)', [req.body.username, target_path, new Date()], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });

                                        connection.query('INSERT INTO distributor_telephone (username, telephone, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.telephone, new Date()], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });

                                        connection.query('INSERT INTO distributor_email (username, email, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.email, new Date()], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });

                                        connection.query('INSERT INTO distributor_line (username, line_id, date_added) VALUES ($1, $2, $3)', [req.body.username, req.body.line_id, new Date()], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });

                                        connection.query('INSERT INTO distributor_parent (username, username_root, date_added) VALUES ($1, $2, $3)', [req.body.username, req.session.nameuser, new Date()], (error, results) => {
                                            if (error) {
                                              throw error
                                            }
                                        });

                                        res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                        res.redirect('/admin/me');
                                    });
                                }
                                
                            } else {
            
                                res.locals.errors = {
                                    "message": 'เกิดข้อผิดพลาดในระบบแล้ว'
                                }
            
                                return res.render(req.renderPage);
            
                            }
                          });
                    });
                    src.on('error', function(err) { 
                        res.cookie('flash_register_message_error', 'Save Data Error !!',{maxAge:3000});
                        res.redirect('/register');
                    });
                }
      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(30).required(),
            email: Joi.string().allow('', null),
            password: Joi.string().min(6).max(30).required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers WHERE id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/customers',{page_title:"User Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE customers set name=?, password =?, telephone =?, line_id =?, updated_at=?  WHERE id = ?";
 
                var query = connection.query(sql, [req.body.name, req.body.password, req.body.telephone, req.body.line_id, new Date(), req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router