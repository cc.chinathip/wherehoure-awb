const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');

router.route('/confirm_orders/:order_id')
.get((req, res, next) => { 

    res.locals.pageData = {
        title:'Register Page'
    }
    res.locals.user = {
        name:'',
        email:'',
        telephone:'',
    }

    
    var order_id = req.params.order_id;
   
    connection.query('SELECT * FROM orders INNER JOIN order_status ON orders.order_status = order_status.status_id INNER JOIN courier ON orders.shipping_code = courier.courier_code WHERE orders.order_id = $1 AND orders.warehouse_id = $2',[order_id, req.session.wh_id],function(err,result) {

        //console.log('result page orders',result)

        connection.query('SELECT order_items.*,products.product_name FROM order_items LEFT JOIN products ON order_items.product_sku = products.product_sku WHERE order_items.order_id = $1',[order_id],function(err,orderItem) {
            connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,whid) {
                connection.query('SELECT * FROM courier',function(err,list_courier) {
                    connection.query('SELECT * FROM order_status',function(err,list_order_status) {
                        if(err){
                            //console.log('error page',err)
                            return res.render('pages/orders/confirm_orders',{page_title:"Order Detail",data:''});   
                        }else{
                            return res.render('pages/orders/confirm_orders',{page_title:"Order Detail",data:result,itemorder:orderItem,warehouse:whid,courier:list_courier,statusorder:list_order_status,order_id:result.rows[0].order_id,username:result.rows[0].username})    
                        }                      
                    });
                });
            });
        });
    });
})
router.route('/detail/:order_id')
.get((req, res, next) => { 

    res.locals.pageData = {
        title:'Register Page'
    }
    res.locals.user = {
        name:'',
        email:'',
        telephone:'',
    }
    //console.log(req.params.order_id)    
    var order_id = req.params.order_id;
    connection.query('SELECT * FROM orders INNER JOIN order_status ON orders.order_status = order_status.status_id INNER JOIN courier ON orders.shipping_code = courier.courier_code WHERE orders.order_id = $1 AND orders.warehouse_id = $2',[order_id, req.session.wh_id],function(err,result) {
        connection.query('SELECT order_items.*,products.product_name FROM order_items LEFT JOIN products ON order_items.product_sku = products.product_sku WHERE order_items.order_id = $1',[order_id],function(err,orderItem) {
            connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,whid) {
                if(err){
                    console.log('error page',err)
                    return res.render('pages/orders/detail',{page_title:"Order Detail",data:''});   
                }else{
                    return res.render('pages/orders/detail',{page_title:"Order Detail",data:result,itemorder:orderItem,warehouse:whid})    
                }                      
            });
        });
    });
})
router.route('/invoice/:order_id')
.get((req, res, next) => { 

    res.locals.pageData = {
        title:'Register Page'
    }
    res.locals.user = {
        name:'',
        email:'',
        telephone:'',
    }
    //console.log(req.params.order_id)    
    var order_id = req.params.order_id;

    connection.query('SELECT * FROM orders INNER JOIN order_status ON orders.order_status = order_status.status_id INNER JOIN courier ON orders.shipping_code = courier.courier_code WHERE orders.order_id = $1 AND orders.warehouse_id = $2',[order_id, req.session.wh_id],function(err,result) {
        connection.query('SELECT order_items.*,products.product_name FROM order_items LEFT JOIN products ON order_items.product_sku = products.product_sku WHERE order_items.order_id = $1',[order_id],function(err,orderItem) {
            connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,whid) {
                if(err){
                    console.log('error page',err)
                    return res.render('pages/orders/invoice',{page_title:"Order Detail",data:''});   
                }else{
                    return res.render('pages/orders/invoice',{page_title:"Order Detail",data:result,itemorder:orderItem,warehouse:whid})    
                }                      
            });
        });
    });    
})

        router.route('/add_orders')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            /* req.renderPage = "pages/orders/form_orders"       
            next() */
            connection.query('SELECT * FROM order_status',function(err,status) {
                console.log('status',status)
            connection.query('SELECT * FROM products',function(err,rows) {
                
                if(err){
                 req.flash('error', err); 
                 res.render('pages/orders/form_orders',{page_title:"orders List",data:''});   
                }else{
                 res.render('pages/orders/form_orders',{page_title:"orders List",data:rows, admin:req.session.adminid,name:req.session.nameuser, data_status:status.rows});
    
                }                      
            });
        });
    })

        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            res.render('pages/orders/form_orders',{name:req.session.nameuser});   
        })
        router.route('/lists')
        .get((req, res, next) => { 
            res.locals.pageData = {
                title:'รายการจัดส่งสินค้า'
            }  
            res.locals.name = {
                message:req.session.nameuser
            }
    
            
    
            connection.query('SELECT * FROM orders INNER JOIN order_status ON orders.order_status = order_status.status_id INNER JOIN courier ON orders.shipping_code = courier.courier_code LEFT JOIN order_items ON order_items.order_id = orders.order_id LEFT JOIN products ON order_items.product_sku = products.product_sku',function(err,result) {
                //connection.query('SELECT O.invoice, CONVERT(date,O.OrderDate) AS Date, P.product_name, I.quantity FROM [purchases] O JOIN purchases_items I ON O.id = I.purchase_id JOIN products P ON P.product_sku = I.product_sku ORDER BY O.invoice',function(err,rows) {
            //connection.query('SELECT * FROM purchases ORDER BY id ASC',function(err,rows) {
                if(err){
                console.log(err)
                 res.render('pages/orders/list_orders',{data:''});    
                }else{
                 res.render('pages/orders/list_orders',{page_title:"รายการสั่งซื้อสินค้า",data:result, admin:req.session.adminid,name:req.session.nameuser});
    
                }                      
            });
    
        })

    router.route('/list_orders')
    .get((req, res, next) => { 

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);

        res.locals.pageData = {
            title:'orders'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM orders',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/orders/list_orders',{page_title:"orders List",data:''});   
            }else{
             res.render('pages/orders/list_orders',{page_title:"orders List",data:rows, admin:req.session.adminid,name:req.session.nameuser});

            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        /* console.log('order insert',req) */

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            warehourse: Joi.required(),
            name_product: Joi.required(),
            qty_product: Joi.required(),
            price_product_one: Joi.required(),
            price_product: Joi.required(),
            total_price: Joi.required(),
            order_status: Joi.required(),
          /*   product_sku: Joi.required(),
            product_price: Joi.required(),
            product_qty: Joi.required(), */
        });

        Joi.validate(data, schema, (err, value) => {

            let name_product_array = req.body.name_product;
            let qty_product_array = req.body.qty_product;
            let price_product_one_array = req.body.price_product_one;
            let price_product_array = req.body.price_product;

            /* let name_product_join = name_product_array.join();
            let qty_product_join = qty_product_array.join();
            let price_product_one_join = price_product_one_array.join();
            let price_product_join = price_product_array.join(); */
            

            //console.log('order insert',value,' ',name_product_join)

            if (err) {

                res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);

            } else {
                
                console.log('insert orders')
               /*  const orders_save = {
                    order_status:req.body.order_status,
                    order_date:new Date(),
                    shipped_date:new Date(),
                    created_at:new Date(),
                  
                } */

                if(req.body.total_price > 0){

                    total = req.body.total_price;
                    
                 }else{

                    total = 0;
                 }
                 
                    connection.query('SELECT order_id FROM orders ORDER BY order_id DESC LIMIT 1',function(err,rows) {
                        
                        let inv = 'INV000'+rows.rows[0].order_id;
                    
                        connection.query('INSERT INTO orders (total_price,order_invoice,order_status, order_date, shipped_date,created_at,warehouse_id,shipping_cost,username) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *', [total,inv,req.body.order_status, new Date(), new Date(), new Date(),1,1,'1'] , (error, results) => {
                            
                            if (error) {
                                throw error
                            }else{
                                
                                for(var i = 0; i < name_product_array.length;i++){

                                    connection.query('INSERT INTO order_items (item_id,product_id, quantity, price_total, order_id,created_at) VALUES ($1, $2, $3, $4, $5, $6)', [1,name_product_array[i], qty_product_array[i], price_product_array[i], results.rows[0].order_id, new Date()], (error, results) => {

                                        console.log('insert order_items',error)
                                    
                                        try {
                                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                                            res.redirect('/admin/me');
                                        } catch (error) {
                                            console.error(error);
                                        }

                                    });
                                    
                                }
                            }
                        });

                });
      
            }
        });
    })

    router.route('/update_order').post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            order_id: Joi.required(),
            shipping_status: Joi.required(),
            courier: Joi.required(),
            shipping_cost:Joi.required(),
            qr_start:Joi.required(),
            qr_end:Joi.required(),
            username:Joi.required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {

                res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);

            } else {

                var sql = "UPDATE orders SET order_status=$1,shipping_code=$2,shipping_cost=$3  WHERE order_id = $4 RETURNING order_id";
                connection.query(sql, [req.body.shipping_status, req.body.courier, req.body.shipping_cost, req.body.order_id], function(error, result) {
                    
                    if (error) {

                        //console.log('error= ',error)
                        throw error

                    }else{

                        //console.log('return order= ',result.rows[0].order_id)
                        let = last_order_id = result.rows[0].order_id;
                        
                        console.log('return order= ',last_order_id)

                        connection.query('UPDATE items SET item_order_id=$1,item_username=$2 WHERE item_sn BETWEEN $3 AND $4;', [last_order_id,req.body.username, req.body.qr_start, req.body.qr_end], function(error, result) {

                            console.log('error= ',error)

                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                             //res.redirect('/admin/me');

                             res.redirect('/orders/detail/'+last_order_id)

                        });
                    }
                    
                });
               
            }
        });
    })



    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(30).required(),
            email: Joi.string().allow('', null),
            password: Joi.string().min(6).max(30).required(),
            telephone:Joi.string().min(6).max(10).required(),
            line_id:Joi.string().allow('', null),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers WHERE id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/customers',{page_title:"User Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE customers set name=?, password =?, telephone =?, line_id =?, updated_at=?  WHERE id = ?";
 
                var query = connection.query(sql, [req.body.name, req.body.password, req.body.telephone, req.body.line_id, new Date(), req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router