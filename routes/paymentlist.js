const express = require('express');
const router = express.Router();
const app = require('http').createServer(handler);
const io = require('socket.io').listen(app);
const fs = require('fs');
const mysql = require('mysql');
const connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'noderegister'
});

var POLLING_INTERVAL = 3000,
pollingTimer;
var connectionsArray = [];

// creating the server ( localhost:8000 )
app.listen(8000);

// on server started we can load our client.html page
function handler(req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    fs.readFile(__dirname + '/client.html', function(err, data) {
      if (err) {
        console.log(err);
        res.writeHead(500);
        return res.end('Error loading client.html');
      }
      res.writeHead(200);
      res.end(data);
    });
  }


var pollingLoop = function() {

    // Doing the database query
    var query = connection.query('SELECT p.id AS payment_id, c.name AS name, p.amount, c.username, c.password, c.telephone As telephone, p.bank, p.slip, p.status, p.	created_at, p.updated_at, ad.name AS admin_name FROM payments p LEFT JOIN customers c ON(p.customer_id = c.id) LEFT JOIN admins ad ON(p.admin_id = ad.id) WHERE admin_id = 0 ORDER BY p.id DESC'),
      users = []; // this array will contain the result of our db query
  
    // setting the query listeners
    query
      .on('error', function(err) {
        // Handle error, and 'end' event will be emitted after this as well
        console.log(err);
        updateSockets(err);
      })
      .on('result', function(user) {
        // it fills our array looping on each user row inside the db
        console.log(user);
        users.push(user);
      })
      .on('end', function() {
        // loop on itself only if there are sockets still connected
        if (connectionsArray.length) {
  
          pollingTimer = setTimeout(pollingLoop, POLLING_INTERVAL);
  
          updateSockets({
            users: users
          });
        } else {
  
          console.log('The server timer was stopped because there are no more socket connections on the app')
  
        }
      });
  };

  var updateSockets = function(data) {
    // adding the time of the last update
    data.time = new Date();
    console.log('Pushing new data to the clients connected ( connections amount = %s ) - %s', connectionsArray.length , data.time);
    // sending new data to all the sockets connected
    connectionsArray.forEach(function(tmpSocket) {
      tmpSocket.volatile.emit('notification', data);
    });
  };

  // creating a new websocket to keep the content updated without any AJAX request
io.sockets.on('connection', function(socket) {

    console.log('Number of connections:' + connectionsArray.length);
    // starting the loop only if at least there is one user connected
    if (!connectionsArray.length) {
      pollingLoop();
    }
  
    socket.on('disconnect', function() {
      var socketIndex = connectionsArray.indexOf(socket);
      console.log('socketID = %s got disconnected', socketIndex);
      if (~socketIndex) {
        connectionsArray.splice(socketIndex, 1);
      }
    });
  
    console.log('A new socket is connected!');
    connectionsArray.push(socket);
  
  });

router.route('/')
    .get((req, res, next) => { 

        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8000');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', true);

        res.locals.pageData = {
            title:'Payments'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT p.id AS payment_id, c.name AS name, p.amount, c.username, c.password, c.telephone As telephone, p.bank, p.slip, p.status, p.created_at, p.updated_at, ad.name AS admin_name FROM payments p LEFT JOIN customers c ON(p.customer_id = c.id) LEFT JOIN admins ad ON(p.admin_id = ad.id) WHERE admin_id <> 0 ORDER BY p.id DESC',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/paymentlist',{page_title:"Payment List",data:''});   
            }else{
             res.render('pages/paymentlist',{page_title:"Payment List",data:rows, admin:req.session.adminid});
            }                      
        });
})
 
module.exports = router