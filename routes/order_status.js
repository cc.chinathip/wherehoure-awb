const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');

        router.route('/')
        .get((req, res, next) => { 
            res.locals.pageData = {
                title:'สถานะสินค้า'
            }  
            res.locals.name = {
                message:req.session.nameuser
            }
            connection.query('SELECT * FROM order_status ORDER BY status_id ASC',function(err,rows) {
                if(err){
                req.flash('error', err); 
                res.render('pages/order_status/listorders_status',{page_title:"สถานะสินค้า",data:''});    
                }else{
                res.render('pages/order_status/listorders_status',{page_title:"สถานะสินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});
                }                      
            });

        })

        router.route('/create')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'สถานะสินค้า'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            req.renderPage = "pages/order_status/addForm"       
            next()
        })
        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            res.render('pages/order_status/addForm',{name:req.session.nameuser});   
        })


    router.route('/lists')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'สถานะสินค้า'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM order_status ORDER BY status_id ASC',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/order_status/listorders_status',{page_title:"สถานะสินค้า",data:''});    
            }else{
             res.render('pages/order_status/listorders_status',{page_title:"สถานะสินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});
            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            status_name: Joi.string().required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {
                      
                        connection.query('INSERT INTO order_status (status_name, created_at) VALUES ($1, $2)', [req.body.status_name ,new Date()], (error, results) => {
                            if (error) {
                              throw error
                            }
                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                            res.redirect('/order_status/lists');
                        });

      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            category_name: Joi.string().min(3).max(30).required(),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM categories WHERE brand_id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/categories',{page_title:"Categories Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE categories set category_name = ?  WHERE category_id = ?";
 
                var query = connection.query(sql, [req.body.category_name, req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'แก้ไขข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router