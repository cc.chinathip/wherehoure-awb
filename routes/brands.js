const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');


        router.route('/create')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            req.renderPage = "pages/brands/addForm"       
            next()
        })
        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            res.render('pages/brands/addForm',{name:req.session.nameuser});   
        })


    router.route('/lists')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'แบรนด์สินค้า'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM brands ORDER BY brand_id ASC',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/brands/ListsofBrands',{data:''});    
            }else{
             res.render('pages/brands/ListsofBrands',{page_title:"แบรนด์สินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});

            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            brand_name: Joi.string().required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {
                if (!req.file) {
                    res.locals.errors = {
                        "message": 'กรุณาอัพโหลดรูปแบรนด์สินค้า'
                    }
                    res.locals.user = req.body
                    return res.render(req.renderPage);
                } else {
                    var tmp_path = req.file.path;
                    var target_path = 'uploads/' + req.file.originalname;
                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);
                    src.on('end', function() { 
                        
                        connection.query('INSERT INTO brands (brand_name, brand_image, created_at) VALUES ($1, $2, $3)', [req.body.brand_name ,target_path,new Date()], (error, results) => {
                            if (error) {
                              throw error
                            }
                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                            res.redirect('/brands/lists');
                        });

                    });
                    src.on('error', function(err) { 
                        res.cookie('flash_register_message_error', 'Save Data Error !!',{maxAge:3000});
                        res.redirect('pages/brands/ListsofBrands');
                    });
                }
      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            brand_name: Joi.string().min(3).max(30).required(),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM brands WHERE brand_id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/brands',{page_title:"Brands Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE brands set brand_name=?  WHERE brand_id = ?";
 
                var query = connection.query(sql, [req.body.brand_name, req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'แก้ไขข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router