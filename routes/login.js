const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const md5 = require('md5');
const {  connection } = require('../config/db');

 
router.route('/')
    .all((req, res, next) => { 

       
        res.locals.pageData = {
            title:'Login Page'
        }
        res.locals.user = {
            email:req.cookies.email || '',
            password:req.cookies.password || '',
            remember:req.cookies.remember || ''
        }
        req.renderPage = "pages/login"
        next()
    })
    .get((req, res, next) => { 
        if(req.cookies.flash_message){
            res.locals.success = {
                message:req.cookies.flash_message
            }
        }
        // res.cookie('my_ck1','cookie_1',{signed:true})
        res.render('pages/login')    
    })
    .post(validation(schema.login), (req, res, next) => { 

       // console.log('validation',req)
        let MAX_AGE = 1000 * 60 * 60 * 24 * 15
        if(req.body.remember){
            req.session.email = req.body.email
            req.session.password = req.body.password
            req.session.remember = req.body.remember
            req.session.cookie.maxAge = MAX_AGE
        }else{
            delete req.session.email
            delete req.session.password
            delete req.session.remember
        }

        let password_hash = md5(req.body.password);

        connection.query('SELECT * FROM users WHERE users_name = $1 AND password = $2', [req.body.email, req.body.password] , (error, results) => {
           // console.log('check connect',error)
            if (error) {
              throw error
            }
            if (results.rows.length > 0) {
                req.session.isLogined = true;
                req.session.adminid = results.rows[0].users_id;
                req.session.nameuser = results.rows[0].users_name;
                req.session.wh_id = results.rows[0].warehouse_id
				res.redirect('/dashboard');
			} else {
				res.locals.errors = {
                    "message": 'Username Or Password not valid !!'
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            }
          })
    })
 
module.exports = router