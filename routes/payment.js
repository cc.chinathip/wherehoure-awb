const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const mysql = require('mysql');

const connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'noderegister'
});

var type = upload.single('image');
 
router.route('/')
    .all((req, res, next) => { 

        res.locals.pageData = {
            title:'Payment'
        }

        connection.query('SELECT * FROM customers ORDER BY id DESC',function(err,rows)     {
            if(err){
                 req.flash('error', err); 
                 res.render('pages/payment',{name:req.session.nameuser,data:''});   
            }else{
                 res.render('pages/payment',{name:req.session.nameuser,data:rows});
            } 
        });  
    })

    router.route('/add')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            customer_id: Joi.string().min(1).required(),
            bank: Joi.string().min(2).max(30).required(),
            amount: Joi.string().min(1).max(30).required()
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM customers ORDER BY id DESC',function(err,rows)     {
                    if(err){
                         req.flash('error', err); 
                         return res.render('pages/payment',{name:req.session.nameuser,data:''});   
                    }else{
                         return res.render('pages/payment',{name:req.session.nameuser,data:rows});
                    } 
                });
            } else {
                if (!req.file) {
                    res.locals.errors = {
                        "message": 'Please select an image to upload'
                    }
                    res.locals.user = req.body

                    connection.query('SELECT * FROM customers ORDER BY id DESC',function(err,rows)     {
                        if(err){
                            req.flash('error', err); 
                            return res.render('pages/payment',{name:req.session.nameuser,data:''});   
                        }else{
                            return res.render('pages/payment',{name:req.session.nameuser,data:rows});
                        } 
                    });
                } else {
                    var tmp_path = req.file.path;
                    var target_path = 'uploads/' + req.file.originalname;
                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);
                    src.on('end', function() { 
                        const post_save = {
                            customer_id:req.body.customer_id,
                            amount:req.body.amount,
                            bank:req.body.bank,
                            tel:'',
                            date_time:'0000-00-00 00:00:00',
                            admin_id:0,
                            status:0,
                            slip:target_path,
                            created_at:new Date(),
                            updated_at:new Date(),
                        }

                        connection.query('INSERT INTO payments SET ?',post_save,(err) => {
                            res.cookie('flash_register_message', 'Save Data Complete',{maxAge:3000});
                            res.redirect('/admin/paymentlist');
                        });
                    });
                    src.on('error', function(err) { 
                        res.cookie('flash_register_message_error', 'Save Data Error !!',{maxAge:3000});
                        res.redirect('/payment');
                    });
                }         
            }
        })
    });

module.exports = router