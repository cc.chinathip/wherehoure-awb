const express = require('express');
const router = express.Router();
const { validation, schema } = require('../validator/users');
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const Pool = require('pg').Pool
const {  connection } = require('../config/db');

router.route('/')
.get((req, res, next) => { 
    
    res.locals.pageData = {
        title:'ตั้งค่า'
    }  
    res.locals.name = {
        message:req.session.nameuser
    }
    //console.log(req.session.wh_id)
    //let wh_id = '1';
    connection.query('SELECT * FROM warehouses WHERE warehouse_id = $1',[req.session.wh_id],function(err,rows) {
            if(err){
            //req.flash('error', err); 
            console.log('error', err)
            res.render('pages/settings',{page_title:"ตั้งค่า",data:''});    
            }else{
                //console.log(result)
                let datas = {
                                
                    page_title:"ตั้งค่า",
                    data:rows, 
                    admin:req.session.adminid,
                    name:req.session.nameuser,
                }

                /* res.setHeader('Content-Type', 'application/json'); */
                res.render('pages/settings',datas);

            }                      
    });
       
})

.post((req, res, next) => { 
    //console.log(req.body)
    const Joi = require('joi');
    const data = req.body;
    const schema = Joi.object().keys({
        warehouse_taxid:Joi.string().allow(''),
        warehouse_token:Joi.string().allow(''),
    });   
    res.locals.pageData = {
        title:'ตั้งค่า'
    }  
    res.locals.name = {
        message:req.session.nameuser
    }

    
    Joi.validate(data, schema, (err, value) => {
        if (err) {
            console.log('error', err)
        } else {
                connection.query('UPDATE warehouses SET warehouse_taxid = $1, warehouse_token = $2 WHERE warehouse_id = $3',[req.body.warehouse_taxid,req.body.warehouse_token,req.session.wh_id],function(err,rows) {
                    if(err){
                        console.log('error', err)
                        res.render('pages/settings'); 
                    }else{
                        res.redirect('admin/settings');
                    }                      
                });
            } 
    });      
})


module.exports = router