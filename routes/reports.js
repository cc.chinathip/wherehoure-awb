const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');
const helpers = require('../helpers/newlib');

router.route('/')
.get((req, res, next) => { 
    var date = new Date();
    var month = date.getMonth();
    date.setDate(1);
    var all_days = [];

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);


    connection.query('SELECT CAST(created_at AS varchar(10)), SUM(total_price) AS totals FROM orders WHERE created_at >= $1 AND created_at <= $2 GROUP BY CAST(created_at AS varchar(10))', [formatDate(firstDay), formatDate(lastDay)], function(err, results){
    if(err){
        console.log(err);
        req.flash('error', err); 
    }else{
        var count_day = formatDate(lastDay).substring(8);  
        var date_notnull = [];
        var total_price = [];    

        if(results.rows.length > 0){
            for (var l = 0; l < results.rows.length; l++) {
                var created_ated = results.rows[l].created_at.substring(8);
                date_notnull.push(created_ated);
            }
                
            for ( var i = 1; i <= count_day; i++ ) {

                if(date_notnull.indexOf(i.toString()) != -1){
                    var key = date_notnull.indexOf(i.toString());
                    total_price.push(results.rows[key].totals);
                } else {
                    total_price.push(0);
                }
                                 
            }

        }
                      
        //console.log(total_price);
        res.render('pages/reports/index',{data:total_price, admin:req.session.adminid,adminname:req.session.nameuser, level:req.session.level,code:req.session.code});
    }  
       
});

});

    router.route('/lists')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'รายงานยอดขาย'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM categories ORDER BY category_id ASC',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/categories/ListsofCategories',{page_title:"หมวดหมู่สินค้า",data:''});    
            }else{
             res.render('pages/categories/ListsofCategories',{page_title:"หมวดหมู่สินค้า",data:rows, admin:req.session.adminid,name:req.session.nameuser});
            }                      
        });

    })

module.exports = router