const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/'});
const fs = require('fs');
const md5 = require('md5');
const {  connection } = require('../config/db');
var type = upload.single('image');

router.route('/')
.get((req, res, next) => { 

    res.locals.pageData = {
        title:'ระบบบริหารจัดการคลังสินค้า'
    }
    res.locals.user = {
        name:'',
        email:'',
        telephone:'',
    }

    req.renderPage = "pages/dashboard"       
    next()
})
.post((req, res, next) => { 
    
    res.locals.pageData = {
        title:'รายชื่อบริษัทขนส่ง'
    }  
    res.locals.name = {
        message:req.session.nameuser
    }
    let list = '1,2';
    //console.log(req.session.wh_id)
    //let wh_id = '1';
    connection.query('SELECT * FROM warehouses WHERE warehouse_id = '+req.session.wh_id+'',function(err,result) {
        connection.query('SELECT * FROM orders WHERE warehouse_id = '+req.session.wh_id+' AND order_id IN (1,2)',function(err,rows) {
            if(err){
            //req.flash('error', err); 
            console.log('error', err)
            res.render('pages/label/index',{page_title:"พิมพ์ใบปะหน้า",data:''});    
            }else{
                //console.log(result)
                let datas = {
                                
                    page_title:"พิมพ์ใบปะหน้า",
                    data:rows, 
                    results:result,
                    admin:req.session.adminid,
                    name:req.session.nameuser,
                }

                /* res.setHeader('Content-Type', 'application/json'); */
                res.render('pages/label/index',datas);

            }                      
        });
    });
       
})

        router.route('/create')
        .all((req, res, next) => { 

            res.locals.pageData = {
                title:'Register Page'
            }
            res.locals.user = {
                name:'',
                email:'',
                telephone:'',
            }

            req.renderPage = "pages/courier/addForm"       
            next()
        })
        .get((req, res, next) => { 
            if(req.cookies.flash_register_message){
                res.locals.register_success = {
                    message:'Save Data Successful'
                }
            }
            if(req.cookies.flash_register_message_error){
                res.locals.errors = {
                    message:'Save Not Successful'
                }
            }

            res.render('pages/courier/addForm',{name:req.session.nameuser});   
        })


    router.route('/lists')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'รายชื่อบริษัทขนส่ง'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM courier ORDER BY courier_id ASC',function(err,rows) {
            if(err){
             req.flash('error', err); 
             res.render('pages/courier/listcourier',{page_title:"รายชื่อบริษัทขนส่ง",data:''});    
            }else{
             res.render('pages/courier/listcourier',{page_title:"รายชื่อบริษัทขนส่ง",data:rows, admin:req.session.adminid,name:req.session.nameuser});
            }                      
        });

    })

    
    router.route('/add')
    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({

            courier_name: Joi.required(),
            courier_code: Joi.required(),
            courier_url_tracking: Joi.required(),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body
                return res.render(req.renderPage);
            } else {
                if (!req.file) {
                    res.locals.errors = {
                        "message": 'กรุณาอัพโหลดรูป Logo'
                    }
                    res.locals.user = req.body
                    return res.render(req.renderPage);
                } else {
                    var tmp_path = req.file.path;
                    var target_path = 'uploads/' + req.file.originalname;
                    var src = fs.createReadStream(tmp_path);
                    var dest = fs.createWriteStream(target_path);
                    src.pipe(dest);
                    src.on('end', function() { 
                        
                        connection.query('INSERT INTO courier (courier_name, courier_code, courier_logo, courier_url_tracking, created_at) VALUES ($1, $2, $3, $4, $5)', [req.body.courier_name, req.body.courier_code, target_path, req.body.courier_url_tracking ,new Date()], (error, results) => {
                           
                            //console.log('results product',results)
                            if (error) {
                              throw error
                            }
                            res.cookie('flash_register_message', 'บันทึกข้อมูลเรียบร้อย',{maxAge:3000});
                            res.redirect('/courier/lists');
                        });

                    });
                    src.on('error', function(err) { 
                        res.cookie('flash_register_message_error', 'Save Data Error !!',{maxAge:3000});
                        res.redirect('pages/courier/addForm');
                    });
                }
      
            }
        });
    })

    router.route('/update')

    .post(type, (req, res, next) => { 

        res.locals.name = {
            message:req.session.nameuser
        }

        const Joi = require('joi');

        const data = req.body;

        const schema = Joi.object().keys({
            category_name: Joi.string().min(3).max(30).required(),
            id:Joi.string().allow('', null),
        });

        Joi.validate(data, schema, (err, value) => {
            if (err) {
                 res.locals.errors = {
                    "message": err.details[0].message
                }
                res.locals.user = req.body

                connection.query('SELECT * FROM categories WHERE brand_id = ?', [req.body.id],function(err,rows)     {

                    return res.render('pages/categories',{page_title:"Categories Detail",data:rows});
                            
                });

            } else {

                var sql = "UPDATE categories set category_name = ?  WHERE category_id = ?";
 
                var query = connection.query(sql, [req.body.category_name, req.body.id], function(err, result) {
                    res.cookie('flash_register_message', 'แก้ไขข้อมูลเรียบร้อย',{maxAge:3000});
                    res.redirect('/admin/me');
                });
               
            }
        });
    })

module.exports = router