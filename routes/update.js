const express = require('express');
const router = express.Router();
const mysql = require('mysql');
const connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'noderegister'
});

router.route('/')
    .get((req, res, next) => { 
        res.locals.pageData = {
            title:'Dashboard Page'
        }  
        res.locals.name = {
            message:req.session.nameuser
        }
        connection.query('SELECT * FROM user ORDER BY user_id DESC',function(err,rows)     {
            if(err){
             req.flash('error', err); 
             res.render('pages/dashboard',{page_title:"User List",data:''});   
            }else{
                
                res.render('pages/dashboard',{page_title:"User List",data:rows});
            }                      
        });
    })
 
router.route('/logout')
    .get((req, res, next) => { 
        delete req.session.isLogined
        res.cookie('flash_message', 'Logout Complete!!',{maxAge:3000});
        res.redirect('/admin/login');    
    })    
 
module.exports = router