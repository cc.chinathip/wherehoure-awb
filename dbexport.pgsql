--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: article; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.article (
    article_id integer NOT NULL,
    article_name character varying(255),
    article_category_id integer,
    article_description character varying(255),
    article_status integer,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    article_image character varying(255),
    deleted integer
);


ALTER TABLE public.article OWNER TO postgres;

--
-- Name: article_article_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.article ALTER COLUMN article_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.article_article_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: brands; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.brands (
    brand_id integer NOT NULL,
    brand_name character varying(255) NOT NULL,
    is_active integer NOT NULL,
    brand_image character varying(255) DEFAULT NULL::character varying,
    created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    CONSTRAINT brands_brand_id_check CHECK ((brand_id > 0))
);


ALTER TABLE public.brands OWNER TO postgres;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categories (
    category_id integer NOT NULL,
    category_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    CONSTRAINT categories_category_id_check CHECK ((category_id > 0))
);


ALTER TABLE public.categories OWNER TO postgres;

--
-- Name: courier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.courier (
    courier_id integer NOT NULL,
    courier_name character varying(255),
    courier_code character varying(255),
    courier_image character varying(255) DEFAULT NULL::character varying,
    courier_url_tracking character varying(255) DEFAULT NULL::character varying,
    created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    updated_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    CONSTRAINT courier_courier_id_check CHECK ((courier_id > 0))
);


ALTER TABLE public.courier OWNER TO postgres;

--
-- Name: distributor_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_address (
    id integer NOT NULL,
    username character varying(255),
    address1 character varying(255),
    address2 character varying(255),
    city_id integer,
    city character varying(255),
    state_id integer,
    state character varying(255),
    country_id integer,
    country character varying(255),
    postcode character varying(50),
    "default" integer,
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_address OWNER TO postgres;

--
-- Name: distributor_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_address ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_code; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_code (
    id integer NOT NULL,
    username character varying(255),
    code character varying(255),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_code OWNER TO postgres;

--
-- Name: distributor_code_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_code ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_email; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_email (
    id integer NOT NULL,
    username character varying(255),
    email character varying(255),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_email OWNER TO postgres;

--
-- Name: distributor_email_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_email ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_file; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_file (
    id integer NOT NULL,
    username character varying(255),
    file character varying(255),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_file OWNER TO postgres;

--
-- Name: distributor_file_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_file ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_file_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_level (
    id integer NOT NULL,
    username character varying(255),
    level integer
);


ALTER TABLE public.distributor_level OWNER TO postgres;

--
-- Name: distributor_level_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_level ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_level_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_line; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_line (
    id integer NOT NULL,
    username character varying(255),
    line_id character varying(255),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_line OWNER TO postgres;

--
-- Name: distributor_line_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_line ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_line_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_login; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_login (
    id integer NOT NULL,
    username character varying(255),
    password character varying(255),
    firstname character varying(255),
    lastname character varying(255),
    status integer,
    date_added timestamp with time zone,
    image character varying(255)
);


ALTER TABLE public.distributor_login OWNER TO postgres;

--
-- Name: distributor_login_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_login ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_login_id_seq
    START WITH 1
    INCREMENT BY 2
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_parent; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_parent (
    id integer NOT NULL,
    username character varying(255),
    username_root character varying(255),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_parent OWNER TO postgres;

--
-- Name: distributor_parent_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_parent ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_parent_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: distributor_telephone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.distributor_telephone (
    id integer NOT NULL,
    username character varying(255),
    telephone character(20),
    date_added timestamp with time zone
);


ALTER TABLE public.distributor_telephone OWNER TO postgres;

--
-- Name: distributor_telephone_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.distributor_telephone ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.distributor_telephone_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: level; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.level (
    id integer NOT NULL,
    level_id integer,
    level_name character varying(255),
    cost character varying(255),
    retail_price character varying(255),
    percent character varying(255),
    income character varying(255),
    total_income character varying(255),
    percent_income character varying(255),
    prodcut_sku character varying(250)
);


ALTER TABLE public.level OWNER TO postgres;

--
-- Name: order_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_items (
    product_sku character varying(255),
    quantity integer,
    price_total integer,
    discount numeric(4,2) DEFAULT 0,
    order_id integer,
    item_id integer,
    created_at timestamp with time zone,
    id integer NOT NULL
);


ALTER TABLE public.order_items OWNER TO postgres;

--
-- Name: order_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.order_status (
    id integer NOT NULL,
    status_id integer,
    status_name character varying,
    create_at timestamp without time zone,
    status_span character varying
);


ALTER TABLE public.order_status OWNER TO postgres;

--
-- Name: orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orders (
    username character varying(255),
    username_parent character varying(255),
    order_status integer,
    order_date date,
    shipped_date date,
    warehouse_id integer,
    level integer,
    invoice_tax character varying(255),
    order_invoice character varying(150),
    identification_code character varying(150),
    parent_code character varying(150),
    total_price integer,
    type_product integer,
    send_type character varying(50),
    send_to character varying(150),
    shipping_firstname character varying(255),
    shipping_lastname character varying(255),
    shipping_address character varying(255),
    shipping_address2 character varying(255),
    shipping_city character varying(255),
    shipping_state character varying(255),
    shipping_country character varying(255),
    shipping_postcode character varying(255),
    shipping_telephone character varying(255),
    shipping_cost integer,
    created_at timestamp with time zone,
    order_id integer NOT NULL,
    shipping_code character varying
);


ALTER TABLE public.orders OWNER TO postgres;

--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    product_name character varying(255),
    brand_id integer,
    category_id integer,
    product_price integer,
    product_qty integer,
    product_image character varying(255),
    created_at timestamp with time zone,
    product_sku character varying(255),
    product_id integer NOT NULL
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: purchases; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchases (
    warehouse_id integer,
    invoice character varying,
    order_status integer,
    created_at timestamp without time zone,
    total_price character varying(255),
    shipping_firstname character varying(255),
    shipping_lastname character varying(255),
    shipping_address character varying(255),
    shipping_address2 character varying(255),
    shipping_city character varying(255),
    shipping_state character varying(255),
    shipping_country character varying(255),
    shipping_postcode character varying(255),
    shipping_telephone character varying(255),
    order_date character varying(255),
    id integer NOT NULL
);


ALTER TABLE public.purchases OWNER TO postgres;

--
-- Name: purchases_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.purchases ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.purchases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: purchases_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.purchases_items (
    purchase_id integer,
    quantity integer,
    totalprice character varying(250),
    created_at timestamp without time zone,
    product_sku character varying(200),
    id integer NOT NULL
);


ALTER TABLE public.purchases_items OWNER TO postgres;

--
-- Name: purchases_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.purchases_items ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.purchases_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    users_id integer NOT NULL,
    users_name character varying(250),
    password character varying(250),
    warehouse_id integer
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: warehouses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.warehouses (
    warehouse_id integer NOT NULL,
    warehouse_name character varying(255),
    warehouse_phone character varying(255),
    warehouse_email character varying(255),
    warehouse_address character varying(255),
    warehouse_district character varying(255),
    warehouse_amphoe character varying(255),
    warehouse_province character varying(255),
    warehouse_zipcode character varying(255),
    warehouse_taxid character varying(255)
);


ALTER TABLE public.warehouses OWNER TO postgres;

--
-- Data for Name: article; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.article (article_id, article_name, article_category_id, article_description, article_status, created_at, updated_at, article_image, deleted) FROM stdin;
1	11	10		1	2020-08-24 11:37:03.173+07	\N	s8-plus-samsung-tempered-glass-samsung-galaxy-s8-plus-by-ok999-shop-1499940027-45801623-e6f6ca899312bba799f20aaabfaa32a5-zoom.jpg	\N
2	2222	10		1	2020-08-24 11:39:42.558+07	\N	176733abc1af07f285ab4d21f6be4c41.jpg	\N
4	2222	11		1	2020-08-24 11:41:57.189+07	\N	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
6	sumsung	13		1	2020-08-24 15:28:49.692+07	2020-08-25 15:39:43.521+07	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
5	2222	12		1	2020-08-24 15:28:15.895+07	2020-08-25 15:40:28.321+07	s8-plus-samsung-tempered-glass-samsung-galaxy-s8-plus-by-ok999-shop-1499940027-45801623-e6f6ca899312bba799f20aaabfaa32a5-zoom.jpg	\N
15	2222	9		1	2020-08-26 13:56:39.006+07	\N	s8-plus-samsung-tempered-glass-samsung-galaxy-s8-plus-by-ok999-shop-1499940027-45801623-e6f6ca899312bba799f20aaabfaa32a5-zoom.jpg	\N
16	2222	12		1	2020-08-26 14:00:47.356+07	\N	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
17	2222	10		1	2020-08-26 14:02:12.663+07	\N	s8-plus-samsung-tempered-glass-samsung-galaxy-s8-plus-by-ok999-shop-1499940027-45801623-e6f6ca899312bba799f20aaabfaa32a5-zoom.jpg	\N
18	apple1	10		1	2020-08-26 14:05:10.565+07	\N	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
19	11	12		1	2020-08-26 14:06:32.965+07	\N	176733abc1af07f285ab4d21f6be4c41.jpg	\N
20	11	12		1	2020-08-26 14:08:32.968+07	\N	176733abc1af07f285ab4d21f6be4c41.jpg	\N
21	zzzz	9		1	2020-08-26 14:20:24.335+07	\N	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
22	2222	12		1	2020-08-26 14:21:20.781+07	\N	c06bfcf5a481008d7504e72c731ee39d.jpg	\N
\.


--
-- Data for Name: brands; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.brands (brand_id, brand_name, is_active, brand_image, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categories (category_id, category_name, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: courier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.courier (courier_id, courier_name, courier_code, courier_image, courier_url_tracking, created_at, updated_at) FROM stdin;
1	Kerry Express	KER	assets/images/courier/	https://th.kerryexpress.com/th/track/?track=	\N	\N
2	SCG Express	scg	assets/images/courier/scg.svg	\N	\N	\N
\.


--
-- Data for Name: distributor_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_address (id, username, address1, address2, city_id, city, state_id, state, country_id, country, postcode, "default", date_added) FROM stdin;
\.


--
-- Data for Name: distributor_code; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_code (id, username, code, date_added) FROM stdin;
\.


--
-- Data for Name: distributor_email; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_email (id, username, email, date_added) FROM stdin;
\.


--
-- Data for Name: distributor_file; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_file (id, username, file, date_added) FROM stdin;
\.


--
-- Data for Name: distributor_level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_level (id, username, level) FROM stdin;
\.


--
-- Data for Name: distributor_line; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_line (id, username, line_id, date_added) FROM stdin;
\.


--
-- Data for Name: distributor_login; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_login (id, username, password, firstname, lastname, status, date_added, image) FROM stdin;
\.


--
-- Data for Name: distributor_parent; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_parent (id, username, username_root, date_added) FROM stdin;
\.


--
-- Data for Name: distributor_telephone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.distributor_telephone (id, username, telephone, date_added) FROM stdin;
\.


--
-- Data for Name: level; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.level (id, level_id, level_name, cost, retail_price, percent, income, total_income, percent_income, prodcut_sku) FROM stdin;
1	1	VIP Dealer	450	990	22.5	540	562.5	\N	LFR
2	2	Dealer	472.5	990	22.5	517.5	540	55	LFR
3	3	Agent	495	990	22.5	495	517.5	52	LFR
4	4	VIP Distributor	517.5	990	0	472.5	472.5	48	LFR
5	5	Distributor	540	990	0	450	450	45	LFR
6	6	Drop Shipping	653	990	0	338	337.5	34	LFR
\.


--
-- Data for Name: order_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_items (product_sku, quantity, price_total, discount, order_id, item_id, created_at, id) FROM stdin;
lfr	1000	10000	0.00	1	\N	\N	1
lfr	1000	10000	0.00	2	\N	\N	2
\.


--
-- Data for Name: order_status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.order_status (id, status_id, status_name, create_at, status_span) FROM stdin;
1	0	รอยืนยัน	\N	badge-info
2	1	จัดส่งแล้ว	\N	badge-success
3	2	กำลังจัดส่ง	\N	badge-warning
4	3	ยกเลิก	\N	badge-danger
5	4	สินค้าตีกลับ	\N	badge-default
\.


--
-- Data for Name: orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orders (username, username_parent, order_status, order_date, shipped_date, warehouse_id, level, invoice_tax, order_invoice, identification_code, parent_code, total_price, type_product, send_type, send_to, shipping_firstname, shipping_lastname, shipping_address, shipping_address2, shipping_city, shipping_state, shipping_country, shipping_postcode, shipping_telephone, shipping_cost, created_at, order_id, shipping_code) FROM stdin;
awb	\N	1	2020-07-04	2020-07-04	1	\N	\N	\N	awbg	\N	10000	1	\N	\N	สมใจ	ดั่งใจหวัง	1199 ปีวรรณทาวเวอร์ ชั้น 2	ถ.พหลโยธิน	พญาไท	พญาไท	กรุงเทพฯ	10400	020263581	\N	\N	1	scg
awb	\N	1	2020-07-05	2020-07-04	1	\N	\N	\N	awbg	\N	10000	1	\N	\N	สมใจ	ดั่งใจหวัง	1199 ปีวรรณทาวเวอร์ ชั้น 2	ถ.พหลโยธิน	พญาไท	พญาไท	กรุงเทพฯ	10400	020263581	\N	\N	2	scg
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (product_name, brand_id, category_id, product_price, product_qty, product_image, created_at, product_sku, product_id) FROM stdin;
LISH Flora	1	1	\N	\N	\N	\N	lfr	1
LISH ATTA	1	1	\N	\N	\N	\N	latt	2
\.


--
-- Data for Name: purchases; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.purchases (warehouse_id, invoice, order_status, created_at, total_price, shipping_firstname, shipping_lastname, shipping_address, shipping_address2, shipping_city, shipping_state, shipping_country, shipping_postcode, shipping_telephone, order_date, id) FROM stdin;
1	INV0001	1	2020-08-28 13:51:14.919	33333	กหดกห	หกดหกดหกด	ดหกดหกดหกดหก	หกดหกดหดหกดหก	พญาไท	พญาไท	กรุงเทพฯ	10400	0874874874	\N	1
1	INV0001	1	2020-08-28 13:53:47.673	33333	กหดกห	หกดหกดหกด	ดหกดหกดหกดหก	หกดหกดหดหกดหก	พญาไท	พญาไท	กรุงเทพฯ	10400	0874874874	\N	2
1	INV0001	1	2020-08-28 13:57:54.607	33333	กหดกห	หกดหกดหกด	ดหกดหกดหกดหก	หกดหกดหดหกดหก	พญาไท	พญาไท	กรุงเทพฯ	10400	0874874874	\N	3
1	INV0001	1	2020-08-28 13:58:52.795	33333	กหดกห	หกดหกดหกด	ดหกดหกดหกดหก	หกดหกดหดหกดหก	พญาไท	พญาไท	กรุงเทพฯ	10400	0874874874	\N	4
1	INV0001	1	2020-08-28 14:00:04.15	33333	กหดกห	หกดหกดหกด	ดหกดหกดหกดหก	หกดหกดหดหกดหก	พญาไท	พญาไท	กรุงเทพฯ	10400	0874874874	\N	5
\.


--
-- Data for Name: purchases_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.purchases_items (purchase_id, quantity, totalprice, created_at, product_sku, id) FROM stdin;
4	4	3960	2020-08-28 13:58:52.809	lfr	1
5	4	3960	2020-08-28 14:00:04.164	lfr	2
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (users_id, users_name, password, warehouse_id) FROM stdin;
1	demo	1234	1
\.


--
-- Data for Name: warehouses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.warehouses (warehouse_id, warehouse_name, warehouse_phone, warehouse_email, warehouse_address, warehouse_district, warehouse_amphoe, warehouse_province, warehouse_zipcode, warehouse_taxid) FROM stdin;
1	AWB	020263581	info@awbg.co.th	1199 Piyavan Tower	พญาไท	พญาไท	กรุงเทพฯ	10400	5154785541254
\.


--
-- Name: article_article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.article_article_id_seq', 22, true);


--
-- Name: distributor_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_address_id_seq', 1, false);


--
-- Name: distributor_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_code_id_seq', 1, false);


--
-- Name: distributor_email_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_email_id_seq', 1, false);


--
-- Name: distributor_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_file_id_seq', 1, false);


--
-- Name: distributor_level_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_level_id_seq', 1, false);


--
-- Name: distributor_line_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_line_id_seq', 1, false);


--
-- Name: distributor_login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_login_id_seq', 1, false);


--
-- Name: distributor_parent_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_parent_id_seq', 1, false);


--
-- Name: distributor_telephone_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.distributor_telephone_id_seq', 1, false);


--
-- Name: purchases_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.purchases_id_seq', 5, true);


--
-- Name: purchases_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.purchases_items_id_seq', 2, true);


--
-- Name: article article_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (article_id);


--
-- Name: brands brands_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.brands
    ADD CONSTRAINT brands_pkey PRIMARY KEY (brand_id);


--
-- Name: courier courier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.courier
    ADD CONSTRAINT courier_pkey PRIMARY KEY (courier_id);


--
-- Name: distributor_address distributor_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_address
    ADD CONSTRAINT distributor_address_pkey PRIMARY KEY (id);


--
-- Name: distributor_code distributor_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_code
    ADD CONSTRAINT distributor_code_pkey PRIMARY KEY (id);


--
-- Name: distributor_email distributor_email_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_email
    ADD CONSTRAINT distributor_email_pkey PRIMARY KEY (id);


--
-- Name: distributor_file distributor_file_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_file
    ADD CONSTRAINT distributor_file_pkey PRIMARY KEY (id);


--
-- Name: distributor_level distributor_level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_level
    ADD CONSTRAINT distributor_level_pkey PRIMARY KEY (id);


--
-- Name: distributor_line distributor_line_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_line
    ADD CONSTRAINT distributor_line_pkey PRIMARY KEY (id);


--
-- Name: distributor_login distributor_login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_login
    ADD CONSTRAINT distributor_login_pkey PRIMARY KEY (id);


--
-- Name: distributor_parent distributor_parent_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_parent
    ADD CONSTRAINT distributor_parent_pkey PRIMARY KEY (id);


--
-- Name: distributor_telephone distributor_telephone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.distributor_telephone
    ADD CONSTRAINT distributor_telephone_pkey PRIMARY KEY (id);


--
-- Name: level level_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.level
    ADD CONSTRAINT level_pkey PRIMARY KEY (id);


--
-- Name: order_items order_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_items
    ADD CONSTRAINT order_items_pkey PRIMARY KEY (id);


--
-- Name: order_status order_status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (id);


--
-- Name: orders orders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (order_id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (product_id);


--
-- Name: purchases_items purchases_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchases_items
    ADD CONSTRAINT purchases_items_pkey PRIMARY KEY (id);


--
-- Name: purchases purchases_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.purchases
    ADD CONSTRAINT purchases_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (users_id);


--
-- Name: warehouses warehouses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.warehouses
    ADD CONSTRAINT warehouses_pkey PRIMARY KEY (warehouse_id);


--
-- PostgreSQL database dump complete
--

