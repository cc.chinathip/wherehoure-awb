const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const store = '';
const { authorize } = require('./config/auth');
const createError = require('http-errors');
const port = 2000;
app.set('port', port);
  
const indexRouter = require('./routes/index');
const loginRouter = require('./routes/login');
const registerRouter = require('./routes/register');
const dashboardRouter = require('./routes/dashboard');
const productsRouter = require('./routes/products');
const ordersRouter = require('./routes/orders');
const brandsRouter = require('./routes/brands');
const categoriesRouter = require('./routes/categories');
const reportsRouter = require('./routes/reports');
const purchasesRouter = require('./routes/purchases');
const usersRouter = require('./routes/users');
const settingsRouter = require('./routes/settings');

var http      = require('http').createServer(app);
var io          = require('socket.io')(http);
const {  connection } = require('./config/db');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('view options', {delimiter: '?'});


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(cookieParser());

//app.set('trust proxy', 1) // trust first proxy
app.use(session({
    name:'sid',
    secret: 'my ses secret',
    store:store,
    resave: true,
    saveUninitialized: true
}))
 
app.use('/', indexRouter);
app.use('/login', authorize('/dashboard', false), loginRouter);
app.use('/register', authorize('/login', true), registerRouter);
app.use('/dashboard', authorize('/login', true), dashboardRouter);
app.use('/dashboard/products', authorize('/login', true), productsRouter);
app.use('/dashboard/orders', authorize('/login', true), ordersRouter);
app.use('/dashboard/brands', authorize('/login', true), brandsRouter);
app.use('/dashboard/categories', authorize('/login', true), categoriesRouter);
app.use('/dashboard/purchases', authorize('/login', true), purchasesRouter);
app.use('/dashboard/users', authorize('/login', true), usersRouter);
app.use('/dashboard/reports', authorize('/login', true), reportsRouter);
app.use('/dashboard/settings', authorize('/login', true), settingsRouter);


app.use(function(req, res, next) {
    var err = createError(404)
    next(err)
});
  
app.use(function (err, req, res, next) {
    res.locals.pageData = {
        title:'Error Page'
    }    
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}
  
    res.status(err.status || 500);
    res.render('pages/error');
});
  
http.listen(port, function() {
    console.log(`Example app listening on port ${port}!`)
});

module.exports = app;
