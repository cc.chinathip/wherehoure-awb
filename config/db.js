
const Pool = require('pg').Pool
const connection = new Pool({
	user: 'postgres',
    host: 'localhost',
    database: 'wh',
    password: 'root',
    port: 5432,
})
module.exports = { connection }